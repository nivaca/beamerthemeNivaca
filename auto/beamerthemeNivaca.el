(TeX-add-style-hook
 "beamerthemeNivaca"
 (lambda ()
   (TeX-run-style-hooks
    "tcolorbox")
   (LaTeX-add-xcolor-definecolors
    "bluenivaca"
    "bonenivaca"
    "goldnivaca"
    "rednivaca")
   (LaTeX-add-tcbuselibraries
    "skins, hooks"))
 :latex)

